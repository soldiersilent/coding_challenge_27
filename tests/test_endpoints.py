from fastapi.testclient import TestClient
import pytest
from ..main import app, token_bucket
import time



client = TestClient(app)


###Fixtures###
@pytest.fixture
def reset_token_bucket():
    response = client.post("/debug/reset_token_bucket/")  # Create this endpoint in your app to reset the token bucket
    assert response.status_code == 200


###Endpoint Tests###
def test_unlimited_endpoint():
    response = client.get("/unlimited/")
    assert response.status_code == 200
    assert response.json() == {"status": "Unlimited number of these, awesome!"}


def test_limited_endpoint():
    response = client.get("/limited/")
    assert response.status_code == 200
    assert response.json() == {"status": "Limited number of these, be careful"}


def test_request_token_bucket_enough(reset_token_bucket):
    response = client.get("/limited/token_bucket/")

    assert response.status_code == 200
    assert response.json() == {"status": "Accepted"}


def test_request_token_bucket_not_enough(reset_token_bucket):
    for _ in range(token_bucket.capacity):
        client.get("/limited/token_bucket/")

    response = client.get("/limited/token_bucket/")

    assert response.status_code == 429



def test_request_token_bucket_refill_rate(reset_token_bucket):
    time_to_partially_refill = 1
    time.sleep(time_to_partially_refill)

    response = client.get("/limited/token_bucket/")

    assert response.status_code == 200
    assert response.json() == {"status": "Accepted"}


def test_token_reset(reset_token_bucket):
    for _ in range(token_bucket.capacity + 1):
        client.get("/limited/token_bucket/")

    response = client.get("/limited/token_bucket/")
    assert response.status_code == 429

    response = client.post("/debug/reset_token_bucket/")
    assert response.status_code == 200
    assert response.json() == {"status": "Token bucket reset"}

    response = client.get("/limited/token_bucket/")
    assert response.status_code == 200
    assert response.json() == {"status": "Accepted"}
