from datetime import datetime

class TokenBucket:
    def __init__(self, capacity: int, refill_rate: int):
        self.capacity = capacity
        self.refill_rate = refill_rate
        self.tokens = capacity
        self.last_refill = datetime.now()

    def refill(self):
        now = datetime.now()
        time_elapsed = (now - self.last_refill).total_seconds()
        tokens_to_add = time_elapsed * self.refill_rate
        self.tokens = min(self.tokens + tokens_to_add, self.capacity)
        self.last_refill = now

    def attempt_request(self) -> bool:
        self.refill()
        if self.tokens >= 1:
            self.tokens -= 1
            return True
        else:
            return False

    def reset(self):
        self.tokens = self.capacity
        self.last_refill = datetime.now()

