from fastapi import FastAPI, HTTPException
import logging
from fastapi.middleware.cors import CORSMiddleware
from limiters.token_bucket import TokenBucket

app = FastAPI()

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


token_bucket = TokenBucket(capacity=10, refill_rate=1)


@app.get('/limited/')
def limited_api_call():
    return {"status": "Limited number of these, be careful"}


@app.get("/unlimited/")
def unlimted_api_call():
    return {"status": "Unlimited number of these, awesome!"}


@app.get("/unlimited/token_bucket/")
def unlimited_api_call_token_bucket():
    return {"status": "Unlimited number of these, awesome!"}


@app.get("/limited/token_bucket/")
def limited_api_call_token_bucket():
    if token_bucket.attempt_request():
        return {"status": "Accepted"}
    else:
        raise HTTPException(status_code=429, detail={"status": "Rejected"})


@app.post("/debug/reset_token_bucket/")
def reset_token_bucket():
    token_bucket.reset()
    return {"status": "Token bucket reset"}
